﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survival
{
    public class Button
    {
        public static int WIDTH;
        public static int HEIGHT;
        public static float SCALE = 3;
        Vector2 position;
        Vector2 origin;
        int id;
        string text;
        bool hover;
        Rectangle sourceRectangle;
        Color textColor = Color.Black;

        public Button(string text, int id)
        {
            this.text = text;
            this.id = id;
            WIDTH = Main.buttonTexture.Width;
            HEIGHT = Main.buttonTexture.Height / 3;
            origin = new Vector2(WIDTH / 2, HEIGHT / 2);
            sourceRectangle = new Rectangle(0, HEIGHT, WIDTH, HEIGHT);
        }

        public void Update()
        {
            if (MouseIntersect() && Main.active)
            {
                hover = true;
                sourceRectangle = new Rectangle(0, HEIGHT * 2, WIDTH, HEIGHT);
                textColor = Color.Yellow;
            }
            else
            {
                hover = false;
                sourceRectangle = new Rectangle(0, HEIGHT, WIDTH, HEIGHT);
                textColor = Color.White;
            }
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(Main.buttonTexture, position, sourceRectangle, Color.White, 0f, origin, SCALE, SpriteEffects.None, 0);
            spritebatch.DrawString(Main.font, text, position, textColor, 0f,
                new Vector2(Main.font.MeasureString(text).X / 2, Main.font.LineSpacing / 2), SCALE - 1, SpriteEffects.None, 0);
        }

        public bool Click(int id)
        {
            if (Main.mouse.LeftButton == ButtonState.Pressed && Main.lastMouse.LeftButton == ButtonState.Released &&
                hover && this.id == id)
                return true;
            return false;
        }

        public void SetPosition(Vector2 position)
        {
            this.position = position;
        }

        bool MouseIntersect()
        {
            Vector2 mousePosition = Main.mousePosition;
            Vector2 pos = position - origin * SCALE;
            if (mousePosition.X >= pos.X && mousePosition.X < pos.X + WIDTH * SCALE &&
                mousePosition.Y >= pos.Y && mousePosition.Y < pos.Y + HEIGHT * SCALE)
                return true;
            return false;
        }
    }
}
