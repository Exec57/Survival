﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survival
{
    public class Block
    {
        public static int SIZE = 48;

        protected BlockID id;
        protected Map map;
        protected Rectangle rectangle;
        protected Vector2 position;
        protected Vector2 origin;
        protected bool solid = false;
        int x, y;

        public BlockID oldId = BlockID.None;

        public int lightLevel;
        Color lightColor = Color.White;

        public Point Index
        {
            get { return new Point(x, y); }
        }

        public BlockID ID
        {
            get { return id; }
        }

        public Vector2 Position
        {
            get { return position; }
        }

        public Vector2 Origin
        {
            get { return origin; }
        }

        public bool Solid
        {
            get { return solid; }
        }

        public Rectangle Rectangle
        {
            get { return rectangle; }
        }

        public Block(Map map, int x, int y)
        {
            this.map = map;
            this.x = x;
            this.y = y;

            position = new Vector2(x * SIZE, y * SIZE);
            origin = new Vector2(SIZE / 2, SIZE / 2);
        }

        public virtual void Update(GameTime gameTimes)
        {
            if (lightLevel == 0)
                lightColor = new Color(0, 0, 0, (int)map.LightAlpha);
            else
                lightColor = new Color(20 * (int)map.LightAlpha / 235, 20 * (int)map.LightAlpha / 235, 0,
                        (int)map.LightAlpha - lightLevel * 18);

            rectangle = new Rectangle((int)position.X - (int)origin.X, (int)position.Y - (int)origin.Y, SIZE, SIZE);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (lightLevel > 0 && Map.DEBUG)
            {
                string text = lightLevel.ToString();
                spriteBatch.DrawString(Main.font, text, position, Color.White,
                    0f, new Vector2(Main.font.MeasureString(text).X / 2, Main.font.LineSpacing / 2), 1, SpriteEffects.None, 0);
            }
        }

        public void DrawMask(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.rectTexture, rectangle, lightColor);
        }

        public virtual void OnDestroyed()
        {
            map.UpdateLight();
        }

        public virtual void OnPlaced()
        {
            map.UpdateLight();
        }
    }
}
