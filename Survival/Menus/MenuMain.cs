﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Survival
{
    public class MenuMain : Menu
    {
        public MenuMain()
        {
            AddButton("Play", 0);
            AddButton("Exit", 1);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (ButtonClick(0))
            {
                Main.menu = new MenuLoad();
                Main.map = new Map();
            }

            if (ButtonClick(1))
                Main.exit = true;
        }
    }
}
