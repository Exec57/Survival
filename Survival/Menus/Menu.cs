﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survival
{
    public class Menu
    {
        List<Button> buttons = new List<Button>();
        protected int width;
        protected int height;

        public Menu()
        {
            width = Main.screenWidth;
            height = Main.screenHeight;
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (Button btn in buttons)
                btn.Update();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < width; x += 128)
            {
                for (int y = 0; y < height; y += 128)
                {
                    spriteBatch.Draw(Main.backgroundTexture, new Rectangle(x, y, 128, 128), Color.White);
                }
            }

            spriteBatch.Draw(Main.rectTexture, new Rectangle(0, 0, width, height), new Color(0, 0, 0, 150));


            foreach (Button btn in buttons)
                btn.Draw(spriteBatch);
        }

        protected void AddButton(string text, int id)
        {
            buttons.Add(new Button(text, id));
            UpdatePosition();
        }

        protected bool ButtonClick(int id)
        {
            foreach (Button button in buttons)
            {
                if (button.Click(id))
                    return true;
            }

            return false;
        }

        public void UpdatePosition()
        {
            width = Main.screenWidth;
            height = Main.screenHeight;
            for (int i = 0; i < buttons.Count(); i++)
            {
                float h = buttons.Count * Button.HEIGHT * Button.SCALE * 1.1f;
                float ori = Button.HEIGHT * Button.SCALE / 2;
                buttons[i].SetPosition(new Vector2(width / 2, height / 2 - h / 2 + ori + i * Button.HEIGHT * Button.SCALE * 1.1f));
            }
        }
    }
}
