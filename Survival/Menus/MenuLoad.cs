﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace Survival
{
    public class MenuLoad : Menu
    {
        string topText = "";
        int percent;
        MapInfo mapInfo;

        Map map;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (map == null)
                map = Main.map;

            if (map != null)
            {
                mapInfo = map.Info;
                float per = mapInfo.offset * mapInfo.mulPer;
                percent = (int)Math.Ceiling(per);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            topText = "Generating world...";

            spriteBatch.Draw(Main.rectTexture,
                  new Rectangle(0, 0, width, 20), Color.Black);
            spriteBatch.DrawString(Main.font, topText, new Vector2(width / 2, 10), Color.White,
           0f, new Vector2(Main.font.MeasureString(topText).X / 2, Main.font.LineSpacing / 2), 1, SpriteEffects.None, 0);

            if (map != null)
            {
                spriteBatch.Draw(Main.rectTexture, 
                    new Rectangle(5, height / 2, width - 10, 5), Color.Black);
                spriteBatch.Draw(Main.rectTexture,
                  new Rectangle(5, height / 2, percent * (width - 10) / 100, 5), Color.DarkGreen);
                string text = percent + "%";
                spriteBatch.DrawString(Main.font, text, new Vector2(width / 2, height / 2), Color.White, 0f,
                    new Vector2(Main.font.MeasureString(text).X / 2 * 0.35f, Main.font.LineSpacing / 2 * 0.35f), 0.35f, SpriteEffects.None, 0);
            }
        }
    }
}
